import React from 'react';
import axios from 'axios';
import './ExchangeRatesPanel.css';

export default class ExchangeRatesPanel extends React.Component{
    constructor() {
      super()
      this.state = {
        rateUSD2EURO: 0,
        rateRUB2USD: 0
      }
    }
    // state = {
    //   rateUSD2EURO: 0,
    //   rateRUB2USD: 0
    // }

    componentDidMount(){
      axios.get('https://common.pricegsm.ru/api/exchange-rate/last/', {withCredentials: true})
        .then(res =>{
          const rates = res.data.result;
          const euroItem = rates.filter(item => item.currency.numcode === 978)[0];
          const rubItem = rates.filter(item => item.currency.numcode === 643)[0];
          const rateUSD2EURO =  Number(euroItem.nominal / euroItem.value).toFixed(2)
          const rateRUB2USD =  Number(rubItem.value / rubItem.nominal).toFixed(2)
          this.setState({ rateUSD2EURO });
          this.setState({ rateRUB2USD });
        })
    }

    render(){
      // console.log(this.state.rateUSD2EURO)
      return (
        <div className = 'last-rate'>
          <div className='rate-block'>
            <div className='rate-title'>
              USD/EURO
            </div>
            <div className='rate-value'>
              {this.state.rateUSD2EURO}
            </div>
          </div>

          <div className='rate-block'>
            <div className='rate-title'>
              RUB/USD
            </div>
            <div className='rate-value'>
              {this.state.rateRUB2USD}
            </div>
          </div>
          
        </div>
      )
    }
}
